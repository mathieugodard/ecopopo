<?php get_header(); ?>

<h1>Les actualités des toilettes sans eau potable</h1>

<p>Cette partie est le relais des actualités communiquées par divers structures (journaux, associations, ...). Aucune rédaction originale n'est à l'oeuvre ici.</p>

<div class="row align-items-start">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <div class="col-12 col-sm-6 col-lg-4 col-xl-3">
        <div class="card">
            <div class="bg-image hover-overlay ripple">
                <img
                    src="<?php the_post_thumbnail_url();?>" class="img-fluid"
                />
                <a href="#!">
                    <div class="mask" style="background-color: rgba(251, 251, 251, 0.15)"></div>
                </a>
            </div>
            <div class="card-body">
                <h5 class="card-title"><?php the_title(); ?></h5>
                <div class="btn btn-success"><?= '<a href=" '.carbon_get_post_meta(get_the_ID(),'source') . '">' . carbon_get_post_meta(get_the_ID(),'site') . '</a>'; ?></div>
                <p class="card-text">
                    <?php the_excerpt(); ?>
                </p>
                <a href="<?php the_permalink(); ?>" class="btn btn-primary">Lire la suite</a>
            </div>
        </div>
    </div>

<?php endwhile; else : ?>
    <p>Aucun élément à afficher</p>
<?php endif; ?>

</div>

<?php get_footer(); ?>