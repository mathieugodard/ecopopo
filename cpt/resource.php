<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

/**
 * CPT resource
 */
add_action('init', 'resource_cpt_init');
function resource_cpt_init() {
    // Créer un nouveau type de contenu (post_type)
    register_post_type('resource', [
        'labels' => [
            'name'                  => _x( 'Ressources', 'Post type general name', 'ecopopo' ),
            'singular_name'         => _x( 'Ressource', 'Post type singular name', 'ecopopo' ),
            'menu_name'             => _x( 'Ressources', 'Admin Menu text', 'ecopopo' ),
            'name_admin_bar'        => _x( 'Ressource', 'Add New on Toolbar', 'ecopopo' ),
            'add_new'               => __( 'Ajouter nouvelle', 'ecopopo' ),
            'add_new_item'          => __( 'Ajouter Nouvelle ressource', 'ecopopo' ),
            'new_item'              => __( 'Nouvelle ressource', 'ecopopo' ),
            'edit_item'             => __( 'Modifier la ressource', 'ecopopo' ),
            'view_item'             => __( 'Voir la ressource', 'ecopopo' ),
            'all_items'             => __( 'Toutes les ressources', 'ecopopo' ),
            'search_items'          => __( 'Rechercher des ressources', 'ecopopo' ),
            'parent_item_colon'     => __( 'Ressource parente :', 'ecopopo' ),
            'not_found'             => __( 'Aucune ressource trouvée.', 'ecopopo' ),
            'not_found_in_trash'    => __( 'Aucune ressource trouvée dans la corbeille.', 'ecopopo' ),
            'featured_image'        => _x( 'Image à la une ressource', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'ecopopo' ),
            'set_featured_image'    => _x( 'Définir l\'image à la une', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'ecopopo' ),
            'remove_featured_image' => _x( 'Supprimer l\'image à la une', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'ecopopo' ),
            'use_featured_image'    => _x( 'Utiliser comme image à la une', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'ecopopo' ),
            'archives'              => _x( 'Archives des ressources', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'ecopopo' ),
            'insert_into_item'      => _x( 'Insérer dans une ressource', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'ecopopo' ),
            'uploaded_to_this_item' => _x( 'Uploader dans cette ressource', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'ecopopo' ),
            'filter_items_list'     => _x( 'Filtrer la liste des ressources', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'ecopopo' ),
            'items_list_navigation' => _x( 'Navigation liste des ressources', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'ecopopo' ),
            'items_list'            => _x( 'Liste des ressources', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'ecopopo' ),
        ],
        'menu_icon' => 'dashicons-admin-links',
        'public' => true,
        'has_archive' => true,
        'rewrite' => ['slug' => 'resource'],
        'supports' => ['title', 'editor' , 'thumbnail', 'excerpt'],
    ]);

    register_taxonomy('type', ['resource'], [
        'label' => 'Type',
        'rewrite' => ['slug' => 'type'],
        'hierarchical' => true
    ]);
}


add_action('carbon_fields_register_fields', 'resource_register_fields');
function resource_register_fields() {
    Container::make('post_meta', 'Infos resource')
        ->where('post_type', '=', 'resource')
        ->add_fields([
            Field::make('text', 'link', 'Lien vers la ressource'),
            Field::make('text', 'author', 'Auteur de la ressource'),
            Field::make('text', 'date', 'Date de la ressource'),
        ])
    ;
}