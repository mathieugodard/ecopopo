<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

/**
 * CPT Recipe
 */
add_action('init', 'structures_cpt_init');
function structures_cpt_init() {
    // Créer un nouveau type de contenu (post_type)
    register_post_type('structures', [
        'labels' => [
            'name'                  => _x( 'Structures', 'Post type general name', 'ecopopo' ),
            'singular_name'         => _x( 'Structure', 'Post type singular name', 'ecopopo' ),
            'menu_name'             => _x( 'Structures', 'Admin Menu text', 'ecopopo' ),
            'name_admin_bar'        => _x( 'Structure', 'Add New on Toolbar', 'ecopopo' ),
            'add_new'               => __( 'Ajouter nouvelle', 'ecopopo' ),
            'add_new_item'          => __( 'Ajouter Nouvelle structure', 'ecopopo' ),
            'new_item'              => __( 'Nouvelle structure', 'ecopopo' ),
            'edit_item'             => __( 'Modifier structure', 'ecopopo' ),
            'view_item'             => __( 'Voir structure', 'ecopopo' ),
            'all_items'             => __( 'Toutes les structures', 'ecopopo' ),
            'search_items'          => __( 'Rechercher des structures', 'ecopopo' ),
            'parent_item_colon'     => __( 'Structure parent :', 'ecopopo' ),
            'not_found'             => __( 'Aucune structure trouvée.', 'ecopopo' ),
            'not_found_in_trash'    => __( 'Aucune structure trouvée dans la corbeille.', 'ecopopo' ),
            'featured_image'        => _x( 'Image à la une structure', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'ecopopo' ),
            'set_featured_image'    => _x( 'Définir l\'image à la une', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'ecopopo' ),
            'remove_featured_image' => _x( 'Supprimer l\'image à la une', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'ecopopo' ),
            'use_featured_image'    => _x( 'Utiliser comme image à la une', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'ecopopo' ),
            'archives'              => _x( 'Archives des structures', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'ecopopo' ),
            'insert_into_item'      => _x( 'Insérer dans une structure', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'ecopopo' ),
            'uploaded_to_this_item' => _x( 'Uploader dans cette structure', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'ecopopo' ),
            'filter_items_list'     => _x( 'Filtrer la liste des structures', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'ecopopo' ),
            'items_list_navigation' => _x( 'Navigation liste des structures', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'ecopopo' ),
            'items_list'            => _x( 'Liste des structures', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'ecopopo' ),
        ],
        'menu_icon' => 'dashicons-admin-site-alt3',
        'public' => true,
        'has_archive' => true,
        'rewrite' => ['slug' => 'structures'],
        'supports' => ['title', 'editor', 'thumbnail', 'excerpt'],
    ]);

    register_taxonomy('category_structures', ['structures'], [
        'label' => 'Catégorie',
        'rewrite' => ['slug' => 'category_structures'],
        'hierarchical' => true
    ]);

    register_taxonomy('status', ['structures'], [
        'label' => 'Statut juridique',
        'rewrite' => ['slug' => 'status'],
        'hierarchical' => true
    ]);

    register_taxonomy('departement', ['structures'], [
        'label' => 'Département',
        'rewrite' => ['slug' => 'departement'],
        'hierarchical' => true
    ]);
}

add_action('carbon_fields_register_fields', 'structure_register_fields');
function structure_register_fields() {
    Container::make('post_meta', 'Contacts structure')
        ->where('post_type', '=', 'structures')
        ->add_fields([
            Field::make('text', 'address', 'Adresse postale'),
            Field::make('text', 'website', 'Site web'),
            Field::make('text', 'phone', 'Téléphone'),
            Field::make('text', 'email', 'Email'),
            Field::make( 'map', 'crb_company_location', 'Location' )
                ->set_help_text( 'drag and drop the pin on the map to select location' ),
        ])
    ;

}