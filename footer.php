    <footer class="footer mt-auto py-3 bg-light">

        <div class="row">

<!--            <div class="col">-->

                <div class="row row-cols-1 row-cols-md-3 g-4">
                    <?php if (is_active_sidebar('footer-sidebar')): ?>
                        <?php dynamic_sidebar('footer-sidebar'); ?>
                    <?php endif; ?>
                </div>

<!--            </div>-->

        </div>

    </footer>

    <?php wp_footer(); ?>

</div>
</body>
</html>