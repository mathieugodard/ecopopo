<?php

require_once __DIR__ . '/cpt/structure.php';
require_once __DIR__ . '/cpt/resource.php';

// Mise en place de Carbon Fields
use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action( 'carbon_fields_register_fields', 'crb_attach_theme_options' );
function crb_attach_theme_options() {
    Container::make( 'theme_options', __( 'Theme Options' ) )
        ->add_fields( array(
            Field::make( 'text', 'crb_text', 'Text Field' ),
        ) );
}

add_action( 'after_setup_theme', 'crb_load' );
function crb_load() {
    require_once( 'vendor/autoload.php' );
    \Carbon_Fields\Carbon_Fields::boot();
}

//Chargement du CSS et de JS
add_action('wp_enqueue_scripts', 'ecopopo_enqueue_styles');
function ecopopo_enqueue_styles() {
// Chargement des CSS
    wp_enqueue_style('bootstrap-css', get_template_directory_uri() . '/bootstrap/css/bootstrap.min.css');
    wp_enqueue_style('theme-style',get_template_directory_uri() . '/style.css',['bootstrap-css'],wp_get_theme()->get('Version'));
    wp_enqueue_style('dataTables-style',get_template_directory_uri() . '/DataTables/media/css/jquery.dataTables.min.css',['bootstrap-css','style.css'],wp_get_theme()->get('Version'));
// Chargement des JavaScripts
    wp_enqueue_script('bootstrap-js',get_template_directory_uri() . '/bootstrap/js/bootstrap.min.js',[],wp_get_theme()->get('Version'),true); // Charger le fichier JS avant la fermeture du body
    wp_enqueue_script('jquery-js',get_template_directory_uri() . '/DataTables/media/js/jquery.js',['bootstrap-js'],wp_get_theme()->get('Version'));
    wp_enqueue_script('dataTables-js',get_template_directory_uri() . '/DataTables/media/js/jquery.dataTables.min.js',['bootstrap-js','jquery-js'],wp_get_theme()->get('Version'));
    wp_enqueue_script('tableau-js',get_template_directory_uri() . '/tableau.js',['bootstrap-js','jquery-js','dataTables-js'],wp_get_theme()->get('Version'),true); // Charger le fichier JS avant la fermeture du body
    wp_enqueue_script('isotope-js',get_template_directory_uri() . '/isotope/isotope.pkgd.min.js',['bootstrap-js','jquery-js','dataTables-js','tableau-js'],wp_get_theme()->get('Version'),true); // Charger le fichier JS avant la fermeture du body
    wp_enqueue_script('script-js',get_template_directory_uri() . '/script.js',['bootstrap-js','jquery-js','dataTables-js','tableau-js','isotope-js'],wp_get_theme()->get('Version'),true); // Charger le fichier JS avant la fermeture du body
    wp_localize_script( 'tableau-js', 'vars', ['template_url' => get_template_directory_uri()] );
    ;
}

/**
 * Enregistrer les emplacements de menu
 */
add_action('after_setup_theme', 'ecopopo_register_menus');
function ecopopo_register_menus() {
    register_nav_menu('menu-header', 'Menu Header');
    register_nav_menu('menu-footer', 'Menu Footer');
    register_nav_menu('menu-side', 'Menu Sidebar');
}

/**
 * Activer des fonctionnalités de WordPress
 */
add_action('after_setup_theme', 'ecopopo_add_theme_support');
function ecopopo_add_theme_support() {
    add_theme_support('custom-logo', [
        'height'      => 100,
        'width'       => 400,
        'flex-width' => true,
    ]);
    add_theme_support('html5'); // Générer des balises et attributs HTML5
    add_theme_support('title-tag'); // Générer la balise <title> dans le head
    add_theme_support('post-thumbnails'); // Activer les images à la une
    add_theme_support('custom-header'); // Activer la personnalisation de l'image du header
}

/**
 * Register Custom Navigation Walker
 */
add_action( 'after_setup_theme', 'register_navwalker' );
function register_navwalker(){
    require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';
}

/**
 * Enregistrer les emplacements des sidebars (widgets)
 */
add_action('widgets_init', 'ecopopo_register_sidebars');
function ecopopo_register_sidebars() {
    register_sidebar([
        'id' => 'ressource-sidebar',
        'name' => 'Page ressources',
        'description' => 'Widgets affichés sur la page des ressources',
        'before_widget' => '<div class="card mb-3">',
        'before_title' => '<h3 class="card-header">',
        'after_title' => '</h3><div class="card-body">',
        'after_widget' => '</div></div>',
    ]);
    register_sidebar([
        'id' => 'footer-sidebar',
        'name' => 'Pied de page',
        'description' => 'Widgets affichés sur le pied de page',
        'before_widget' => '<div id="%1$s" class="widget %2$s col"><div class="card col mb-3">',
        'before_title' => '<h3 class="card-header">',
        'after_title' => '</h3><div class="card-body">',
        'after_widget' => '</div></div></div>',
    ]);
}

/**
 * Actualiser les permaliens lorsque l'on active le thème
 */
add_action('after_switch_theme', 'ecopopo_rewrite_flush');
function ecopopo_rewrite_flush() {
    recipes_cpt_init(); // Appeler la fonction qui permet d'ajouter un CPT
    flush_rewrite_rules(); // Actualiser les permaliens
}

add_action('carbon_fields_register_fields', 'article_register_fields');
function article_register_fields()
{
    Container::make('post_meta', 'Infos source')
        ->where('post_type', '=', 'post')
        ->add_fields([
            Field::make('text', 'source', 'Lien vers la source'),
            Field::make('text', 'site', 'Site de la source'),
            Field::make('text', 'author', 'Auteur de la source'),
            Field::make('text', 'date', 'Date de la source'),
        ]);
}



///**
// * Disable Comments
// */
//add_action('admin_init', function () {
//    // Redirect any user trying to access comments page
//    global $pagenow;
//
//    if ($pagenow === 'edit-comments.php') {
//        wp_redirect(admin_url());
//        exit;
//    }
//
//    // Remove comments metabox from dashboard
//    remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
//
//    // Disable support for comments and trackbacks in post types
//    foreach (get_post_types() as $post_type) {
//        if (post_type_supports($post_type, 'comments')) {
//            remove_post_type_support($post_type, 'comments');
//            remove_post_type_support($post_type, 'trackbacks');
//        }
//    }
//});
//
//// Close comments on the front-end
//add_filter('comments_open', '__return_false', 20, 2);
//add_filter('pings_open', '__return_false', 20, 2);
//
//// Hide existing comments
//add_filter('comments_array', '__return_empty_array', 10, 2);
//
//// Remove comments page in menu
//add_action('admin_menu', function () {
//    remove_menu_page('edit-comments.php');
//});
//
//// Remove comments links from admin bar
//add_action('init', function () {
//    if (is_admin_bar_showing()) {
//        remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
//    }
//});


// Fonction qui boucle les taxonomies
function display_terms_btn(int $id, string $taxonomy) {
    $terms = get_the_terms($id, $taxonomy);
    if (is_array($terms)) {
        foreach ($terms as $term) {
            echo '<a href="' . get_term_link($term) . '" class="btn btn-sm">' . $term->name . '</a>'
            ;
        }
    }
}

// Supprime l'item Commentaires dans l'administration car je ne veux pas gérer les commentaires.
// Le code ci-dessus doit-il être décommenté ? Une extension gère très bien celà : Disable comments
// TODO : poser la question à Pierre pour désactiver les commentaires
add_action('admin_menu', 'ecopopo_remove_menu_pages');
function ecopopo_remove_menu_pages()
{
    remove_menu_page('edit-comments.php');
}

// Ajout de l'API Google Maps
add_filter( 'carbon_fields_map_field_api_key', 'crb_get_gmaps_api_key' );
function crb_get_gmaps_api_key( $current_key ) {
    return 'AIzaSyBihUldKhiDQFE7VHRcwaCzbcjQRXF7bw8';
}


// Autoriser Gutenberg dans le CPT Structures
add_filter('register_post_type_args', 'ajout_gut_cpt', 10, 2);
function ajout_gut_cpt($args, $post_type){
    if ($post_type == 'structures'){
        // Reprendre la liste des éléments et ajouter editor
        $args['show_in_rest'] = true;
        // Reprendre la liste des éléments et ajouter editor
        $args['supports'] = array('title', 'thumbnail', 'editor');
    }
    return $args;
}