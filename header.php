<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div class="container">
    <!-- Vérifier si un menu a été associé à l'emplacement menu-header dans l'admin -->
    <?php if (has_nav_menu('menu-header')): ?>

        <header class="header_section">
            <div class="container-fluid">
                <nav class="navbar navbar-expand-lg custom_nav-container bg-light">
                    <a class="navbar-brand" href="index.html">
                        <?php if (has_custom_logo()): ?>
                            <?php the_custom_logo(); ?>
                            <?php bloginfo('description'); ?>
                        <?php else: ?>
                            <?php bloginfo('name'); ?>
                        <?php endif; ?>
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <div class="d-flex ml-auto flex-column flex-lg-row align-items-center">
                            <ul class="navbar-nav">
                                <?php
                                wp_nav_menu( array(
                                    'theme_location'    => 'menu-header',
                                    'depth'             => 2,
//                                    'container'         => 'div',
//                                    'container_class'   => 'collapse navbar-collapse',
//                                    'container_id'      => 'bs-example-navbar-collapse-1',
                                    'menu_class'        => 'nav',
                                    'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                                    'walker'            => new WP_Bootstrap_Navwalker(),
                                ) );
                                ?>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </header>



    <?php endif; ?>
