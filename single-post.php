<?php get_header(); ?>

    <main>
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div class="row">
                <div class="col-sm-4">
                    <?= 'Auteur.trice : ' . carbon_get_post_meta(get_the_ID(),'author'); ?><hr>
                    <?= 'Date : ' . carbon_get_post_meta(get_the_ID(),'date'); ?><hr>
                    <?= 'Source : <a href=" '.carbon_get_post_meta(get_the_ID(),'source') . '">' . carbon_get_post_meta(get_the_ID(),'site') . '</a>'; ?><hr>
                </div>
                <div class="col-sm-8">
                    <h1><?php the_title(); ?></h1>
                    <?php the_post_thumbnail('medium'); ?>
                    <?php the_content(); ?>
                </div>

            </div>
        <?php endwhile; else : ?>
            <p>Aucun élément à afficher</p>
        <?php endif; ?>
    </main>

<?php get_footer(); ?>