<?php
// Template Name: Ressource

get_header(); ?>
<main>
<!--    <div class="row">-->
<!--        <div class="grid" data-isotope='{ "itemSelector": ".grid-item", "layoutMode": "fitRows" }'>-->
<!--            <div class="grid-item">Site Web</div>-->
<!--            <div class="grid-item">Livre</div>-->
<!--            <div class="grid-item">Audio</div>-->
<!--            <div class="grid-item">Image</div>-->
<!--            <div class="grid-item">Tutoriel</div>-->
<!--            <div class="grid-item">Vidéo</div>-->
<!--        </div>-->
<!--    </div>-->

    <?php $query = new WP_Query([ 'post_type' => 'resource' ]); ?>

    <div class="row">
        <h1>Filterer les types de ressources :</h1>
        <div class="button-group filters-button-group">
            <button data-filter="*" class="btn button is-checked" >Montrer tout</button>
            <button data-filter=".livre" class="btn button">Livre</button>
            <button data-filter=".tutoriel" class="btn button">Tutoriel</button>
            <button data-filter=".audio" class="btn button">Audio</button>
            <button data-filter=".video" class="btn button">Vidéo</button>
            <button data-filter=".site-web" class="btn button">Site Web</button>
            <button data-filter=".image" class="btn button">Image</button>
        </div>
    </div>

    <div class="row">
        <div class="grid mt-5">

            <?php while($query->have_posts()) : $query->the_post(); ?>

            <div class="element-item
            <?php
            $types = get_the_terms(get_the_ID(),'type');
            foreach ($types as $type){
                echo $type->slug,' ';
            }
            ?>" data-category="transition">

                <?php the_post_thumbnail('thumbnail'); ?>

                <div class="row">
                    <h5 class="card-title"><?php the_title(); ?>
                        <?php if(carbon_get_the_post_meta('date')){
                        echo '(' . carbon_get_the_post_meta('date') . ')';} ?>
                        </h5>
                </div>

<!--                    <div class="row">-->
<!--                        --><?php //display_terms_btn(get_the_ID(), 'type'); ?>
<!--                    </div>-->

                <div class="row">
                    <p><?= carbon_get_the_post_meta('author'); ?></p>
                </div>

<!--                    <div class="row">-->
<!--                        <p>Année d'édition : --><?//= carbon_get_the_post_meta('date'); ?><!--</p>-->
<!--                    </div>-->
                <p class="card-text"><small class="text-muted"><a href="<?= carbon_get_the_post_meta('link'); ?>" target="_blank">Voir la ressource</a></small></p>
            </div>



            <?php endwhile; ?>
            <?php wp_reset_postdata(); // A mettre après une boucle avec WP_Query ?>
        </div>
    </div>


<!--    Sidebar du template ressource-->
<!--        <div class="col-3">-->
<!---->
<!--            <aside>-->
<!--                --><?php //if (is_active_sidebar('ressource-sidebar')): ?>
<!--                    --><?php //dynamic_sidebar('ressource-sidebar'); ?>
<!--                --><?php //endif; ?>
<!--            </aside>-->
<!---->
<!--        </div>-->


    <div class="row">
        <div class="col">

        <h1>Ressources</h1>
        <p>Vous trouverez ici toutes les ressources intéressantes à propos des toilettes sèches</p>

        <?php $query = new WP_Query([ 'post_type' => 'resource' ]); ?>

            <div class="accordion accordion-flush" id="accordionFlushExample">
                <?php $idi = 1; ?>
                <?php while($query->have_posts()) : $query->the_post(); ?>

                <div class="accordion-item">
                    <?= '<h2 class="accordion-header" id="flush-heading' . $idi . '">'; ?>
                    <?= '<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse' . $idi . '" aria-expanded="false" aria-controls="flush-collapse' . $idi . '">'; ?>

                    <div class="col-3">
                        <?php the_post_thumbnail('thumbnail'); ?>
                    </div>

                    <div class="col-9">
                        <div class="row">
                            <h5 class="card-title"><?php the_title(); ?></h5>
                        </div>

                        <div class="row">
                            <?php display_terms_btn(get_the_ID(), 'type'); ?>
                        </div>

                        <div class="row">
                            <p>Auteur.ice : <?= carbon_get_the_post_meta('author'); ?></p>
                        </div>

                        <div class="row">
                            <p>Année d'édition : <?= carbon_get_the_post_meta('date'); ?></p>
                        </div>
                    </div>

                        </button>
                        </h2>
                        <?= '<div id="flush-collapse' . $idi . '" class="accordion-collapse collapse" aria-labelledby="flush-heading' . $idi . '" data-bs-parent="#accordionFlushExample">'; ?>
                        <div class="accordion-body">
                            <p class="card-text"><?php the_excerpt(); ?></p>
                            <p class="card-text"><small class="text-muted"><a href="<?= carbon_get_the_post_meta('link'); ?>" target="_blank">Voir la ressource</a></small></p>
                        </div>
                </div>

                <?php $idi++; ?>
                <?php endwhile; ?>

                <?php wp_reset_postdata(); // A mettre après une boucle avec WP_Query ?>
            </div>
        </div>
    </div>
</main>

<?php get_footer(); ?>