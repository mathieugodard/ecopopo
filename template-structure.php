<?php
// Template Name: Structures

get_header(); ?>

    <main>
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<!--            <h1>--><?php //the_title(); ?><!--</h1>-->
        <?php endwhile; else : ?>
            <p>Aucun élément à afficher</p>
        <?php endif; ?>


        <?php
        $query = new WP_Query([
            'post_type' => 'structures',
            'orderby' => the_title(),
            'order' => 'DESC',
            'posts_per_page' => 1000
        ]);
        ?>

            <table id="tab" class="display" style="width:100%">
                <thead>
                <tr>
                    <th>Nom</th>
                    <th>Site Web</th>
                    <th>Description</th>
                    <th>Structure</th>
                    <th>Département</th>
                </tr>
                </thead>
                <tbody>

                <?php while($query->have_posts()) : $query->the_post(); ?>
                    <tr>
                        <td><?php the_title(); ?></td>
                        <td><a href="<?= carbon_get_the_post_meta('website'); ?>"><?= carbon_get_the_post_meta('website'); ?></a></td>
                        <td><?php the_content(); ?></td>
                        <td><?php display_terms_btn(get_the_ID(), 'status'); ?></td>
                        <td><?php display_terms_btn(get_the_ID(), 'departement'); ?></td>

                    </tr>
                <?php endwhile; ?>
                <?php wp_reset_postdata(); // A mettre après une boucle avec WP_Query ?>

                </tbody>
            </table>

        <iframe allowfullscreen="" src="//umap.openstreetmap.fr/fr/map/ecopopo_537776?scaleControl=false&amp;miniMap=false&amp;scrollWheelZoom=false&amp;zoomControl=true&amp;allowEdit=false&amp;moreControl=true&amp;searchControl=null&amp;tilelayersControl=null&amp;embedControl=null&amp;datalayersControl=true&amp;onLoadPanel=none&amp;captionBar=true&amp;fullscreenControl=true" width="100%" height="700px" frameborder="0"></iframe>


















    </main>

<?php get_footer(); ?>